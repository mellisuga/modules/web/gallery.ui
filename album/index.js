require("./theme.css");

const XHR = require('core/utils/xhr_async.js');
const ContentNavigator = require('core/paginator/index.js');
const MediaPage = require('./media_page.js');


module.exports = class {
  constructor(elem, navigator, album) {
    this.element = elem;
    this.navigator = navigator;
    this.title = album.title;
    this.description = album.description;
  }

  static async construct(elem, album_name, cfg) {
    try {
      let album_req_params = {
        command: "album_by_id",
        id: album_name
      };
      if (!album_name) {
        album_req_params = {
          command: "random_items",
          category: cfg.category,
          limit: cfg.limit || 10
        }
      }

      let album = false;

      if (album_name && typeof album_name !== "string") {
        album = await XHR.get('/content-manager/gallery', {
          command: "album_by_id",
          id: album_name
        });
        console.log(album);
        album_name = album.title;
      }

      if (!cfg.fit) {
        cfg.fit = {
          max_height: 900
        }
      }

      if (!album_name) {
        cfg.random_items = await XHR.get('/content-manager/gallery', album_req_params);
        cfg.div = elem;
        console.log("cfg", cfg)

        let media_page = await MediaPage.init(
          0, cfg.limit || 10, cfg.limit || 10, false, false, cfg
        );
        await media_page.resize();
      } else {

        console.log("ALBUM", album_name);
        let max_index = cfg.limit || await XHR.get('/content-manager/gallery', {
          command: "max_index",
          album: album_name
        });

        if (!cfg) cfg = {};
        let navigator = await ContentNavigator.init({
          div: elem,
          max_index: max_index,
          page_size: cfg.limit || 6,
        }, async function(index, page_size, max_index, content_navigator) {
          let media_page = await MediaPage.init(
            index, page_size, max_index, content_navigator, album_name, cfg
          );
          await media_page.resize();
          media_page.listen_resize();
          navigator.cur_page.images = media_page.images;
          return media_page; // TODO: remove max_pages argument as it is never used nor will be...
        });
        await navigator.get_page(0, navigator.page_size);
      }
      
      return new module.exports(elem, navigator, album);
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }
}

