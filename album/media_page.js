
const XHR = require('core/utils/xhr_async.js');
const Image = require('./image.js');

//const GridUI = require('core/grid.ui/index.js');


function is_touch_device() {
    
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    
    var mq = function (query) {
        return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}

module.exports = class {
  constructor(cfg, navigation, album) {
    this.album = album;
    this.cfg = cfg.cfg;

    let index = this.index = cfg.index;
    let page_size = this.page_size = cfg.page_size;
    let max_pages = this.max_pages = cfg.max_pages;
    let max_index = this.max_index = cfg.max_index;
    let images = this.images = [];

    this.navigation = navigation;

    let grid_element = this.gride = document.createElement('div');
    grid_element.classList.add("img_grid");
    console.log("APPEND DIV", cfg.cfg.div);
    if (cfg.cfg.div) cfg.cfg.div.appendChild(grid_element);
    if (navigation) navigation.display.appendChild(grid_element);
    
//    let grid_ui = this.grid_ui = new GridUI(2, window.innerWidth, 10, 300, grid_element);
// !!    document.body.appendChild(grid_ui.element);
//    grid_ui.resize(window.innerWidth);

//    let cur_in_row = this.cur_in_row = grid_ui.items_in_row;
    let this_class = this;
  }

  static async init(index, page_size, max_index, navigation, album_name, cfg) {
    try {
      let this_class = new module.exports({
        index: index,
        page_size: page_size,
        max_index: max_index,
        cfg: cfg
      }, navigation, album_name);
      var videos = document.getElementsByTagName("video");

      window.addEventListener('scroll', function(ev) {
        this_class.checkScroll()
      }, false);
      return this_class;
    } catch (e) {
      console.error(e);
      return undefined;
    }
     
  }

  listen_resize() {
    let this_class = this;
    let _this = this;

    this.currentWW = window.innerWidth;

    this.resize_listener = async function(ev) {
      try {
      //  console.log("RESIZED");
      //  console.log(_this.currentWW, "!==", window.innerWidth);
        if (_this.currentWW !== window.innerWidth && !this_class.resizing/* && is_touch_device()*/) {
          await this_class.resize();
/*          if (this_class.resize_again) {
            this_class.resize_again = false;
            await this_class.resize_listener();
          }*/
         // this_class.grid_ui.resize(window.innerWidth);
     /*     let inrow = this_class.grid_ui.items_in_row;
          if (inrow != this_class.cur_in_row) {
            this_class.destroyed = true;
            await this_class.resize(inrow);
            this_class.cur_in_row = inrow;
          }
          this_class.checkScroll();

          this_class.resizing = false;
          console.log("resizing state now", this_class.resizing);
          if (this_class.resize_again) {
            console.log("resize again");
            this_class.resize_again = false;
            await this_class.resize_listener();
          }
          console.log("end");*/
        } else {
          this_class.resize_again = true;
        }
      } catch(e) {
        console.error(e.stack);
      } 
    };
    window.addEventListener('resize', this.resize_listener);

  }


  async resize(inrow) {
    try {
      display_loading_overlay();
      this.resizing = true;

      let _this = this;
      if (!inrow) inrow = this.cur_in_row; else this.cur_in_row = inrow;
      for (let i = 0; i < this.images.length; i++) {
        let imag = this.images[i];
        this.gride.removeChild(imag.element);
      }
      this.images = [];

      if (this.navigation) {
    //    document.body.appendChild(this.grid_ui.element);

        this.max_index = await XHR.get('/content-manager/gallery', {
          command: "max_index",
          album: this.album
        });

        let page_size_navi = this.page_size;

        let max_index = await XHR.get('/content-manager/gallery', {
          command: "max_index",
          album: this.album
        });

        await this.navigation.resize(page_size_navi, max_index);
      }
      await this.load_media();

      let fit_state = this.fit_items();

      if (this.resize_again) {
        this.resize_again = false;
        await this.resize();
      } else {
        hide_loading_overlay()

        this.resizing = false;
      }

      this.currentWW = window.innerWidth;
    } catch (e) {
      console.error(e.stack);
    }
  }

  async load_media() {
    try {
//      let min = Math.max(0, this.index*this.page_size)+1;
 //     let max = min + this.page_size+1;
      let min = Math.max(0, this.max_index - (this.index*this.page_size+this.page_size))+1;
      let max = this.max_index - this.index*this.page_size;
      if (this.index !== 0) {
     //   max += 1;
      };
      console.log("this.index", this.index, this.max_index);

      let offset = Math.max(0, this.max_index - (this.index*this.page_size+this.page_size));
      let limit = this.page_size;
      if (offset == 0 && this.index != 0) {
        limit = (this.max_index%this.page_size);
        if (limit == 0) limit = this.page_size;
      }

      

      console.log("REQ MEDIA PAGE", offset, limit, this.album);
      let media_items = this.cfg.random_items ? this.cfg.random_items : await XHR.get('/content-manager/gallery', {
        command: "page",
        offset: offset,
        limit: limit,
        album: this.album
      });


      let srcs = [];
      for (let i = 0; i < media_items.length; i++) {
        let item = media_items[i];
        srcs.push(item.src);
      }

      for (var i = 0; i < media_items.length; i++) {
        var src = media_items[i];
        if (!src.hidden) {
          let ialbum = this.album || src.album;
          src.src = "/gallery-directory/"+ialbum+"/"+src.name;
          var image = await Image.init(src, this.images, this.navigation, this.cfg);
      /*    let ndiv = document.createElement("div");
          let nimg = document.createElement("img");
          nimg.src = src.src;
          ndiv.appendChild(nimg);*/
          this.gride.appendChild(image.element);

          if (image) {
            this.images.push(image);
        //    this.grid_ui.add(image.element);
          }
        //  this.grid_ui.resize(window.innerWidth);
        }
      }

      for (var i = 0; i < this.images.length; i++) {
        let image = this.images[i];

        if (image.vid) {
          if (isMobile()) {
            image.vid.currentTime = "0.1";
          } else {
            image.vid.play();
          }
        }
      }
      
    } catch (e) {
      console.error(e.stack);
    }
  }
  
  checkScroll() {
    if (!isMobile()) {
      const fraction = 0.2;
      for(var i = 0; i < this.images.length; i++) {
        var image = this.images[i];
        if (image.vid) {
          var video = image.vid;
          var vpo = video.getBoundingClientRect();
          var x = vpo.left, y = vpo.top, r = vpo.right, b = vpo.bottom,
          w = video.offsetWidth, h = video.offsetHeight,
          visibleX, visibleY, visible;

          visibleX = Math.max(0, Math.min(w, window.innerWidth - x, r));
          visibleY = Math.max(0, Math.min(h, window.innerHeight - y, b));

          visible = visibleX * visibleY / (w * h);

          if (visible > fraction) {
            video.play();
          } else {
            video.pause();
          }
        }
      }
    } 
  }

  fit_items() {
    let gbox = this.gride;
    let gitems = gbox.querySelectorAll(".gallery_ui_item");
    let offsets = {}; 
    for (let i = 0; i < gitems.length; i++) {
      let imge = gitems[i].querySelector("img");
      imge.style.maxHeight = "";
      let style = window.getComputedStyle(gitems[i]);
      
      let ml = Math.ceil(parseFloat(style.marginLeft));
      let mr = Math.ceil(parseFloat(style.marginRight));
      let bl = Math.ceil(parseFloat(style.borderLeftWidth));
      let br = Math.ceil(parseFloat(style.borderRightWidth));
      let pl = Math.ceil(parseFloat(style.paddingLeft));
      let pr = Math.ceil(parseFloat(style.paddingRight));
//      let pw = pl + pr;
      let width = ml + bl + pl + Math.ceil(parseFloat(window.getComputedStyle(imge).width)) + pr + br + mr;
      console.log(imge, width);

      if (!offsets[gitems[i].offsetTop]) {
        let mt = Math.ceil(parseFloat(style.marginTop));
        let mb = Math.ceil(parseFloat(style.marginBottom));
        let bt = Math.ceil(parseFloat(style.borderTopWidth));
        let bb = Math.ceil(parseFloat(style.borderBottomWidth));
        let pt = Math.ceil(parseFloat(style.paddingTop));
        let pb = Math.ceil(parseFloat(style.paddingBottom));

        offsets[gitems[i].offsetTop] = {
          height: imge.offsetHeight-(mt+bt+pt+pb+bb+mb),
          width: width,
          imgs: [imge]
        };
      } else {
        offsets[gitems[i].offsetTop].width += width;
        offsets[gitems[i].offsetTop].imgs.push(imge);
      }
      //    console.log(style.marginLeft, style.marginRight, style.borderLeftWidth, style.borderRightWidth, style.paddingLeft, style.paddingRight, style.width);
    }


    document.body.style.overflow = 'visible';
    let gbox_width = gbox.offsetWidth;
    document.body.style.overflow = 'hidden';

    for (let o in offsets) {
      console.log(offsets[o]);
      for (let i = 0; i < offsets[o].imgs.length; i++) {
        let perfect_height = offsets[o].height/offsets[o].width*gbox_width;
        if (perfect_height > this.cfg.fit.max_height && offsets[o].imgs.length == 1) {
          offsets[o].imgs[i].style.maxHeight = this.cfg.fit.max_height+"px";
        } else {
          offsets[o].imgs[i].style.maxHeight = perfect_height+"px";
        }
      }
    }
    return true;
  }
  
  destroy() {
    this.delete_selection_button.removeEventListener('click', this.delete_listener);
    window.removeEventListener('resize', this.resize_listener);

    this.images.forEach(function(imag) {
      imag.destroy();
    });
    this.grid_ui.remove_all();
    this.images = [];
//    document.body.removeChild(this.grid_ui.element);
  }
}

    function isMobile() {
      return window.matchMedia("only screen and (max-width: 760px)").matches;
    }


