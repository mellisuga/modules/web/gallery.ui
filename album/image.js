'use strict'

const XHR = require('core/utils/xhr_async.js');

const display_html = require('./display.html');

module.exports = class {
  constructor(img, obj, images, navigator, cfg) {
    let element = this.element = document.createElement("div");
    this.element.classList.add("gallery_ui_item", "noselect");
    this.element.appendChild(img);

    this.img = img;
    this.images = images;
    this.navigator = navigator;


    if (img.tagName === "VIDEO") {
      this.vid = img;
    }

    let this_class = this;

    img.addEventListener('click', function() {
      if (cfg && cfg.callback && cfg.callback.click) {
        cfg.callback.click(obj);
      } else {
        if (cfg.href) {
          window.location.href = cfg.href;
        } else {
          this_class.display();
        }
      }
    });
    this.src = obj.src;

    this.element.addEventListener("mouseover", function(e) {  
      if (cfg && cfg.callback && cfg.callback.mousein) {
        cfg.callback.mousein(element, obj);
      }
    });

    this.element.addEventListener("mouseleave", function(e) {  
      if (cfg && cfg.callback && cfg.callback.mouseout) {
        cfg.callback.mouseout(element, obj);
      }
    });
    

    if (cfg && cfg.callback && cfg.callback.load) {
      cfg.callback.load(element, obj);
    }
  }
  
  display() {
    this.create_display();
    let display_controls = this.display_element.querySelector(".display_controls");
    let back_btn = this.display_element.querySelector('.display_close');
    let left_btn = this.left_btn, right_btn = this.right_btn;


    const this_index = this.images.indexOf(this);
    if (this_index < 1 && this.navigator.cur_page.index == 0) {
      if (display_controls.contains(left_btn)) {
        display_controls.removeChild(left_btn);
      }
    } else {
      if (!display_controls.contains(left_btn)) {
        display_controls.appendChild(left_btn);
      }
      left_btn.addEventListener("click", this.navigate_left);
    }

    console.log(this.navigator.cur_page.index, ">", this.navigator.max_pages);
    if (this_index > this.images.length-2 && this.navigator.cur_page.index == this.navigator.max_pages-1) {
      if (display_controls.contains(right_btn)) {
        display_controls.removeChild(right_btn);
      }
    } else {
      if (!display_controls.contains(right_btn)) {
        display_controls.appendChild(right_btn);
      }
      right_btn.addEventListener("click", this.navigate_right);
    }

    let this_class = this;

    document.body.style.overflow = "hidden";
    document.body.style.position = "fixed";
    this_class.displayed = true;
 
    let hide_after = 2000; // 1000 miliseconds
    let last_moved = Date.now();

    this.interv = setInterval(function() {
      let now = Date.now();
      if (last_moved < now-hide_after) {
        display_controls.style.display = "none";
      }
    }, 1000);

    this.mouse_moved_listener = function () {
      if (display_controls.style.display === "none") {
        display_controls.style.display = "";
      }
      last_moved = Date.now();
    };

    document.addEventListener('mousemove', this.mouse_moved_listener, false);

    back_btn.addEventListener("click", this.hide_element);

    document.body.appendChild(this.display_element);
    if (this.dvid) {
      this.dvid.play();
    }
  }

  create_display() {
    this.display_element = document.createElement("div");
    this.display_element.classList.add("gallery_ui_display");
    this.display_element.innerHTML = display_html;
    let dimg = this.display_element.querySelector("img"); 
    let dvid = this.display_element.querySelector("video");
    
    let srcs = [];
    
    if (this.img.tagName === "VIDEO") {
      this.dvid = dvid;
    /*  if ("" !== ( dvid.canPlayType( 'video/mp4; codecs="avc1.42E01E"' )
      || dvid.canPlayType( 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"' ) )) {*/
        let vid_src_mp4 = document.createElement('source');
        vid_src_mp4.type = "video/mp4";
        vid_src_mp4.src =  this.src;
        srcs.push(vid_src_mp4);
        dvid.appendChild(vid_src_mp4);
    /*  } else if ("" !== dvid.canPlayType( 'video/webm; codecs="vp8, vorbis"' )) {
        let vid_src_webm = document.createElement('source');
        vid_src_webm.type = "video/webm";
        vid_src_webm.src =  this.src.slice(0, -3)+'webm';
        srcs.push(vid_src_webm);
        dvid.appendChild(vid_src_webm);
      }*/
      dimg.style.display = "none";
    } else {
      dimg.src = this.src;
      dvid.style.display = "none";
    }
        
    let this_class = this;
    
    this.hide_element = function(e) {
      if (this_class.img.tagName === "VIDEO") {
        for (let s = 0; s < srcs.length; s++) {
          srcs[s].src = "";
        }
        dvid.load();
      }

      document.body.style.overflow = "auto";
      document.body.style.position = "";
      this_class.hide(); 
    };

    this.navigate_left = async function(e) {
      const next_index = this_class.images.indexOf(this_class)-1;
      if (next_index > -1 && next_index < this_class.images.length) {
        this_class.images[next_index].display();
        this_class.hide();
      } else {
        this_class.hide();
        await this_class.navigator.select_page(this_class.navigator.cur_page.index-1);
        this_class.navigator.cur_page.images[this_class.navigator.cur_page.images.length-1].display();
      }
    }

    this.navigate_right = async function(e) {
      const next_index = this_class.images.indexOf(this_class)+1;
      if (next_index > -1 && next_index < this_class.images.length) {
        this_class.images[next_index].display();
        this_class.hide();
      } else {
        this_class.hide();
        await this_class.navigator.select_page(this_class.navigator.cur_page.index+1);
        this_class.navigator.cur_page.images[0].display();
      }
    }

    this.left_btn = this.display_element.querySelector('.display_left');
    this.right_btn = this.display_element.querySelector('.display_right');
  }

  hide() { 
    document.body.removeChild(this.display_element);
    this.display_element = false;
    this.displayed = false;
    document.removeEventListener('mousemove', this.mouse_moved_listener, false);
    document.removeEventListener('click', this.hide_element, false);
    document.removeEventListener('click', this.navigate_left, false);
    document.removeEventListener('click', this.navigate_right, false);
    clearInterval(this.interv);
  }

  static async init(obj, images, navigator, cfg) {
    try {
      let src = obj.src;
      return await new Promise(resolve => { 
        if (src.endsWith(".mp4")) {
          let vid = document.createElement('video');
          vid.innerHTML = 'Your browser does not support HTML5 video.';
          vid.muted = 'muted';
          
          let progress_func = function(e) {
            resolve(new module.exports(vid, obj, images, navigator, cfg));
            vid.removeEventListener('progress', progress_func);
          };

          vid.addEventListener('progress', progress_func, false); 
          
          vid.addEventListener('ended', function(e) {
            this.currentTime = 0.1;
            this.play();
          }, false);

    //      if ("" !== ( vid.canPlayType( 'video/mp4; codecs="avc1.42E01E"' )
     //     || vid.canPlayType( 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"' ) )) {
            let vid_src_mp4 = document.createElement('source');
            vid_src_mp4.type = "video/mp4";
            vid_src_mp4.src =  src;
            vid.appendChild(vid_src_mp4);
      /*    } else if ("" !== vid.canPlayType( 'video/webm; codecs="vp8, vorbis"' )) {
            let vid_src_webm = document.createElement('source');
            vid_src_webm.type = "video/webm";
            vid_src_webm.src =  src.slice(0, -3)+'webm';
            vid.appendChild(vid_src_webm);
          }*/

          
        } else {
          var img = document.createElement('img');
          img.src = src;
          img.addEventListener("load", e => {
            resolve(new module.exports(img, obj, images, navigator, cfg));
          });

          img.addEventListener("error", e => {
            console.error(e);
            resolve(undefined);
          });
        }
      });
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }
}
