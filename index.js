
const XHR = require("core/utils/xhr_async.js");

function is_touch_device() {
    
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    
    var mq = function (query) {
        return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
        return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}

module.exports = class {
  static async construct(elem, category, cms_path, cfg) {
    try {
      if (!cms_path) cms_path = "/content_management"

      let albums = await XHR.get("/content-manager/gallery", {
        command: "get_albums_by_category",
        category: category
      });

      let album_path = (cfg && cfg.album_path) ? cfg.album_path : "/Albumas";

      for (let a = 0; a < albums.length; a++) {
        console.log(albums[a]);
        let adiv = document.createElement("a");
        adiv.href = album_path+"?id="+albums[a].id;

        let ah2 = document.createElement("h2");
        ah2.innerHTML = albums[a].title;
        adiv.appendChild(ah2);

        let aimg = document.createElement("img");
        aimg.src = albums[a].cover ? "/gallery-directory/"+encodeURIComponent(albums[a].title)+"/"+albums[a].cover : cms_path+"/g/icons/album256.png";
        adiv.appendChild(aimg);

        
        function limit_chars(str, limit) {
          if (str.length > limit) {
            str = str.substring(0, limit)+"...";
          }
          return str;
        }
      
        let ap = document.createElement("p");
        ap.innerHTML = limit_chars(albums[a].description, 100);
        adiv.appendChild(ap);

        if (!is_touch_device()) {

          ah2.classList.add("hidden");
          ap.classList.add("hidden");

          adiv.addEventListener('mouseover', e => {
            ah2.classList.remove("hidden");
            ap.classList.remove("hidden");
          });

          adiv.addEventListener('mouseleave', e => {
            ah2.classList.add("hidden");
            ap.classList.add("hidden");
          });
        }

        elem.appendChild(adiv);
      }

      let _this = new module.exports(elem);
      
      return _this;
    } catch (e) {
      console.error(e.stack);
    }
  }

  constructor(elem) {
    this.element = elem || document.createElement("div");
  }
}
